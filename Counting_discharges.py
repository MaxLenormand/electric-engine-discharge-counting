"""
Count the number of discharges that occur on the PPT

Webcam used: Logitech C270 (720p, 30fps)
Using i5 8th gen, 8Gb RAM

TO DO:
    get the average pixel value of the frame
    store the average pixel value of the previous frame
    compare the two. if difference > threshold => discharge
    
    in a list: 0 is no discharge, 1 is discharge
    1 sec = 30 frames
    it takes ~7 sec to charge the capacitors
    1 discahrge ~ each 210 frames.
    1 min = 30*60 = 1800 frames
    
    
Using my laptop, I get an average number of frames that are treated seems to be around 16 per second.
I tried using one of the labs laptop, with an i7 (don’t known what gen), which allowed to get 30fps, however the counting was less reliable for some reason. So I sticked to using my laptop.

    
"""

import cv2
import numpy as np
import time
import pickle
#from matplotlib import pyplot as plt


if __name__ == "__main__": 
    
    file_name = str(input('enter the discharge file name : '))
    nb_minutes_to_analyze = int(input('enter the number of minutes to look : '))
    
    cap = cv2.VideoCapture(1)
    #0 referes to the 1st webcam available
    
    discharges = []
    discharge_counter = 0
    
    #output the file and save it:
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640,480))
    
    prev_frame_average_pixel_value = 255
    lower_threshold_pixel_value = 0.7
    upper_threshold_pixel_value = 30
    
    t1=time.time()
    time_since_last_fps_count = t1
    time_of_1_segment = nb_minutes_to_analyze*60
    fps_over_time = {}
    number_of_frames_per_segment = 0
    segment_number= 0
    fps=0
    
    while True:
       ret, frame = cap.read()
       gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
       out.write(frame)
       
       ret, thresh = cv2.threshold(gray, 245, 255, cv2.THRESH_BINARY)
       thresh = cv2.erode(thresh, None, iterations=2)
       thresh = cv2.dilate(thresh, None, iterations=4)
       
       average_pixel_value = np.mean(thresh)
       print('average pixel value', average_pixel_value)
       if (average_pixel_value - prev_frame_average_pixel_value > lower_threshold_pixel_value 
           and average_pixel_value - prev_frame_average_pixel_value < upper_threshold_pixel_value):
           discharge_counter+=1
           discharges.append(1)
           print('\n dicharge! \n')
       else:
           discharges.append(0)
       prev_frame_average_pixel_value = average_pixel_value
       #cv2.imshow('frame', frame)
       cv2.imshow('gray', gray)
       cv2.imshow('thresh', thresh)
       
       timer_to_check_fps = time.time()
       timer_fps = timer_to_check_fps-time_since_last_fps_count
       if timer_fps > time_of_1_segment:
           if fps==0:
               fps = len(discharges)/timer_fps
           else:
               fps = len(discharges[-number_of_frames_per_segment:])/timer_fps
           
           print('\n latest fps is: {} \n'.format(fps))
           fps_over_time[segment_number]=fps
           number_of_frames_per_segment=0
           time_since_last_fps_count = time.time()
           segment_number+=1
           
       number_of_frames_per_segment+=1
       
       if cv2.waitKey(1) & 0xFF== ord('q'):
           #If the key that is pressed is 'q' then break
           break
       
    t2 = time.time()
    time_experiment=t2-t1
    print('\ntime elapsed is {}sec and there were {} discharges'.format(time_experiment, discharge_counter))
    print('average number of frames per second: ',len(discharges)/time_experiment)
    print('average time between discharge is: ', time_experiment/discharge_counter)
    print('the repartition of fps is: \n',fps_over_time)
    
    
    #Recording all the data in files:
    with open(file_name, 'wb')as file:
        """
        stores the discharge file
        """
        my_pickle = pickle.Pickler(file)
        my_pickle.dump(discharges)
        
    with open(file_name+str('_fps_over_time'), 'wb')as file:
        """
        stores the fps_over_time dictionary
        """
        my_pickle = pickle.Pickler(file)
        my_pickle.dump(fps_over_time)
        
    with open(file_name+str('_time_of_experiment'), 'wb')as file:
        """
        stores the time of experiment, which is just a number
        """
        my_pickle = pickle.Pickler(file)
        my_pickle.dump(time_experiment)
    
    with open(file_name+str('_number_of_minutes_of_1_segment'), 'wb')as file:
        """
        stores the nb_minutes_to_analyze, which is just a number
        """
        my_pickle = pickle.Pickler(file)
        my_pickle.dump(nb_minutes_to_analyze)
    
    
    #Be careful, we are reversing the list to get the last frame of discharge!
    discharges.reverse()
    #This is done to find the frame of the last discharge
    for i in range(len(discharges)):
        if discharges[i] == 0:
            pass
        else:
            last_discharge_frame = i
    print('last discharge was at frame {}:'.format(last_discharge_frame))
    time_last_discharge=last_discharge_frame/((len(discharges)/time_experiment)*60*60)
    print('the last discharge happenned {} hours ago'.format(time_last_discharge))
    
    cap.release()
    out.release()
    cv2.destroyAllWindows()