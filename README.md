# Electric engine discharge counting

Set of codes to count the number of discharges over long experiment times of electric thrutsers.
This code was done using a Logitech C270 camera (720p, 30 fps).

There are 2 codes:
- **Counting_discharges**: Run during the experiment, monitors the discharges. Press 'q' to finish.
- **display_dsitribution**: Plots the discharge disctribution once the experiment is over.


The counting discharge code works by using the feed of the webcam, turning it into a gray scale, and applys a mask to only see
pixels above a certain value. Then, at each frame, it counts the average value of all the pixels on that mask, and if the difference from
the average of the frame before it is > threshold, it counts it as a discharge:
if (avg(frame(t+1)) - avg(frame(t))) > threshold => 1 discharge

The second code allows to display the number of discharges over a given period of time:

**Plot examples**

Here is an example of a test done for multiple hours, with a time period of 30min. (So each bar represents the number of discharges over 30min)

![](no_nozzles_june_13_plot.png)

We can see that apart from some differences at the beginning, the discharge rate is actually pretty constant.




Here is another test, with a time period of only 1min (minimum allowed):

![](test_0_deg_nozzle_plot.png)

This test was less succesful, but is interesting to see, we can see the slow degradation in number of discharges at each minute.




It should also be mentionned this was run of a relatively slow spec laptop (i5 8th gen, 8Gb RAM). Since it was not capable of processing
the images at 30fps, I made it so that it counts the average fps processed per time segment, to then display the number of discharges back at the
correct moment.

Feel free to use and modify this code in any way(just a mention would be appreciated if you use it).
Though, it should be noted that this code is not at all optimized, especially after multiple hours, the frame rate starts to drop.
This is probabaly due to RAM overuse, which I did note have time to look into. Therefor, one of the inputs is a time period, which
will in part be used to keep track of the average framerate for that period.

