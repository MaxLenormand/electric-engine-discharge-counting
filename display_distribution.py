"""
Program to display the discharges counted using the previous code
"""
import pickle
from matplotlib import pyplot as plt
#import numpy as np


if __name__ == "__main__":
    file_name = str(input('enter the discharge file name : '))
    
    
    with open(file_name, 'rb')as file:
        my_pickle = pickle.Unpickler(file)
        discharges = my_pickle.load()
       
    with open(file_name+str('_fps_over_time'), 'rb')as file:
        my_pickle = pickle.Unpickler(file)
        fps_over_time = my_pickle.load()    
    
    with open(file_name+str('_time_of_experiment'), 'rb')as file:
        my_pickle = pickle.Unpickler(file)
        time_over_experiment = my_pickle.load()
    
    with open(file_name+str('_number_of_minutes_of_1_segment'), 'rb')as file:
        my_pickle = pickle.Unpickler(file)
        nb_minutes_to_analyze = my_pickle.load()
    
    number_segments=0
    for i in fps_over_time:
        number_segments +=1 
    
    
    ##Uncomment for the May 13th discharge test
    #time_over_experiment = 50114
    #fps=12.91
    
    print('there are {} frames in the discharge list'.format(len(discharges)))    
    list_nb_discharges = []
    
    time_in_sec_of_segment = nb_minutes_to_analyze*60
    number_segments = time_over_experiment//time_in_sec_of_segment
    
    dict_discharge_segments = {}
    
    
    for i in range(int(number_segments)):
        #1st frame of segment
        segment = int(i*time_in_sec_of_segment*fps_over_time[i])
        #print('segment is {} seconds'.format(segment))
        
        #last frame of segment
        next_segment=int((i+1)*time_in_sec_of_segment*fps_over_time[i])
            
        dict_discharge_segments[i] = discharges[segment:next_segment]
        
    for i in dict_discharge_segments:
        nb_discharges=0
        for y in  dict_discharge_segments[i]:
            if y ==1:
                nb_discharges +=1
        print('for segement {} of {} there were {} discharges'.format(i,number_segments, nb_discharges))
        list_nb_discharges.append(nb_discharges)
        if nb_discharges == 0:
            break
    
    print('the list of discharges per segement is: \n', list_nb_discharges)
               
    plt.bar(range(len(list_nb_discharges)), list_nb_discharges)
    plt.title('number of discharges per time segement')
    plt.xlabel('time segements')
    plt.ylabel('number of discharges')
    plt.show()